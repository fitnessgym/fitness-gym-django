# Site for Fitness Gym with Django-framework
HTML-template by [site](https://freehtmlthemes.ru/categories/sport/template-450) 


## Installation
1. Install requirements:

    ```
    source venv/bin/activate
    pip install -r requirements.txt
    ```

2. In the _mysite/mysite_ folder, rename the file ``.example.settings.py`` on `settings.py`
3. Insert your **SECRET KEY** in _settings.py_ (line 23)
4. Insert your **DB settings** in _settings.py_ (line 81-89)



## Run project
Run the command:
* from project root folder ```source venv/bin/activate```
* ```cd mysite```
* ```
  python manage.py migrate
  python manage.py collectstatic
  python manage.py runserver
  ```
