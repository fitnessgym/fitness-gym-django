from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50, db_index=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('category', kwargs={'category_id': self.pk})


class News(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=75)
    content = models.TextField(verbose_name='Контент', blank=True)
    photo = models.ImageField(verbose_name='Фото', upload_to='photos/%Y/%m/%d/', blank=True)
    is_published = models.BooleanField(verbose_name='Опубликовано', default=True)
    category = models.ForeignKey(verbose_name='Категория', to='Category', on_delete=models.PROTECT, null=True)
    views = models.IntegerField(default=0)
    author = models.ForeignKey(User, on_delete=models.PROTECT, default=1)
    created_at = models.DateTimeField(verbose_name='Дата публикации', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Обнавлено', auto_now=True)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-created_at', 'title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('view_news', kwargs={'pk': self.pk})


class NewsComment(models.Model):
    news = models.ForeignKey(News, on_delete=models.PROTECT, default=1)
    user = models.ForeignKey(User, on_delete=models.PROTECT, default=1)
    text = models.TextField()
    created_at = models.DateTimeField(verbose_name='Дата публикации', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Обнавлено', auto_now=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['-created_at']

    def __str__(self):
        return f"{self.news} | {self.user}"
