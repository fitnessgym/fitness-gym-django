from django import forms
from .models import NewsComment
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ckeditor_uploader.fields import RichTextUploadingField


class NewsCommentForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = NewsComment
        fields = ['text', 'user', 'news']
        widgets = {
            'user': forms.HiddenInput(),
            'news': forms.HiddenInput(),
        }
