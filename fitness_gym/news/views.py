from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from .models import News, Category, NewsComment
from django.views.generic import ListView, DetailView
from .forms import NewsCommentForm
from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import F


class NewsList(ListView):
    model = News
    context_object_name = 'news'
    paginate_by = 2
    template_name = 'news/blog.html'

    def get_queryset(self):
        return News.objects.filter(is_published=True).select_related('category')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'News'
        context['sub_title'] = 'All'
        return context


class NewsByCategory(ListView):
    model = News
    context_object_name = 'news'
    allow_empty = False
    paginate_by = 2
    template_name = 'news/blog.html'

    def get_queryset(self):
        return News.objects.filter(category_id=self.kwargs['category_id'], is_published=True).select_related('category')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "News"
        context['sub_title'] = Category.objects.get(pk=self.kwargs['category_id']).title
        return context


class ViewNews(DetailView):
    model = News
    context_object_name = 'news_item'
    template_name = 'news/view_news.html'
    comment_form = NewsCommentForm
    comment_on_page = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "News"
        context['sub_title'] = News.objects.get(pk=self.kwargs['pk']).title
        context['comment_total'] = int(NewsComment.objects.filter(news=self.kwargs['pk']).count())
        context['comment_form'] = self.comment_form

        paginator = Paginator(NewsComment.objects.filter(news=self.kwargs['pk']), self.comment_on_page)
        page_number = self.request.GET.get('page', 1)
        page_obj = paginator.get_page(page_number)

        context['page_obj'] = page_obj

        self.object.views += 1
        self.object.save()
        return context


def add_comment(request):
    if request.method == 'POST':
        form = NewsCommentForm(request.POST)
        if form.is_valid():
            form.cleaned_data['user'] = request.user.pk
            form.cleaned_data['news'] = form.cleaned_data['news'].pk
            form.save()
            messages.success(request, 'Комментарий был опубликован')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', 'home'))
        else:
            messages.error(request, 'Ошибка публикации комментария :(')
