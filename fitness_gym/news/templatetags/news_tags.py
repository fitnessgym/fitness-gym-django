from django import template
from ..models import Category
from django.db.models import Count, F
from django.core.cache import cache


register = template.Library()


@register.inclusion_tag('news/list_categories.html')
def show_categories():
    categories = Category.objects.annotate(cnt=Count('news', filter=F('news__is_published'))).filter(cnt__gt=0)
    total = 0
    for cat in categories:
        total += cat.cnt
    return {"categories": categories, 'total_news': total}


@register.inclusion_tag('news/list_categories.html')
def show_categories_with_cache():
    categories = cache.get('categories')
    if not categories:
        categories = Category.objects.annotate(cnt=Count('news', filter=F('news__is_published'))).filter(cnt__gt=0)
        cache.set('categories', categories, 30)
    return {"categories": categories}
