from django.urls import path
from . import views


urlpatterns = [
    path('', views.NewsList.as_view(), name='news'),
    path('category/<int:category_id>/', views.NewsByCategory.as_view(), name='category'),
    path('<int:pk>/', views.ViewNews.as_view(), name='view_news'),
    path('comment/add/', views.add_comment, name='add-comment'),
]
