from django.contrib import admin
from django import forms
from .models import ClubInformation, ClubContacts, Contacts
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class ClubInformationForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = ClubInformation
        fields = '__all__'


class ClubInformationAdmin(admin.ModelAdmin):
    form = ClubInformationForm
    fields = ('created_at', 'data', 'contacts',)


admin.site.register(ClubInformation, ClubInformationAdmin)
admin.site.register(ClubContacts)
admin.site.register(Contacts)
