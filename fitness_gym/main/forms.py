from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={
        'class': "form-control",
        "placeholder": "Your Name",
    }))
    email = forms.EmailField(label='Сообщени', widget=forms.TextInput(attrs={
        'class': "form-control",
        "placeholder": "Your E-mail",
    }))
    subject = forms.CharField(label='Тема', widget=forms.TextInput(attrs={
        'class': "form-control",
        "placeholder": "Subject",
    }))
    message = forms.CharField(label='Сообщени', widget=forms.Textarea(attrs={
        'class': "form-control",
        "rows": 6,
        "placeholder": "Message",
    }))
