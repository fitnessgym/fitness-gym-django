from django.shortcuts import render, redirect
from .models import ClubInformation, Trainer
from .forms import ContactForm
from django.core.mail import send_mail
from django.contrib import messages
from django.conf import settings


def index(request):
    return render(request, 'main/index.html')


def about(request):
    context = {
        'data': ClubInformation.objects.all().first().data,
        'trainers': Trainer.objects.all(),
        'title': 'About Club',
    }
    return render(request, 'main/about-us.html', context)


def contacts(request):
    fitness_club_info = ClubInformation.objects.all().first()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            mail = send_mail(
                subject=f"{form.cleaned_data['email']} :: {form.cleaned_data['name']} :: {form.cleaned_data['subject']}",
                message=form.cleaned_data['message'],
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[fitness_club_info.contacts.email],
                fail_silently=False,
            )
            if mail:
                messages.success(request, 'Ваше письмо отправлено')
                return redirect('contacts')
            else:
                messages.error(request, 'Ошибка отправки письма')
    else:
        form = ContactForm()

    context = {
        'title': 'Contacts',
        'form': form,
        'club_contacts': fitness_club_info.contacts,
    }

    return render(request, 'main/contacts.html', context)


def feedback(request):
    if request.method.lower() == 'post':
        pass
    else:
        redirect('contacts')
