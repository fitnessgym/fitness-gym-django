from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Contacts(models.Model):
    phone = PhoneNumberField(verbose_name='Телефон', blank=True)
    email = models.EmailField(verbose_name='Email', blank=True)


class ClubContacts(Contacts):
    address = models.CharField(verbose_name='Адрес', max_length=150)
    map = models.TextField(default='<div></div>')


class ClubInformation(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100, default='Fitness Club')
    created_at = models.DateTimeField(verbose_name='Дата создания')
    data = models.TextField(verbose_name='Информация а клубе')
    contacts = models.ForeignKey('ClubContacts', on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class TrainerSpecialty(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=150)


class Trainer(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)
    patronymic = models.CharField(verbose_name='Отчество', max_length=50)
    photo = models.ImageField(verbose_name='Фото', upload_to='photos/team-img/', blank=True)
    specialty = models.ForeignKey('TrainerSpecialty', on_delete=models.PROTECT)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
