from django.apps import AppConfig
from . import COACHES_GROUP_NAME


class ServicesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'services'

    def ready(self):
        from django.contrib.auth.models import Group

        coach_group_users = Group.objects.get_or_create(name=COACHES_GROUP_NAME)
