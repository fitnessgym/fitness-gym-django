from django.shortcuts import render
from django.views.generic import ListView
from .models import Service, Class


class ServicesList(ListView):
    model = Service
    context_object_name = 'services'
    template_name = 'services/about_services.html'

    def get_queryset(self):
        return Service.objects.filter(is_active=True)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Services'
        context['sub_title'] = 'All'
        context['top_classes'] = Class.objects.filter(is_active=True)[:3]
        return context
