from django.contrib.auth.models import User
from django.db import models
from django.core.validators import FileExtensionValidator
from django.db.models import Q
from . import COACHES_GROUP_NAME


class Service(models.Model):
    title = models.CharField(verbose_name='Сервис', max_length=50)
    description = models.TextField(verbose_name='Описание сервиса', max_length=200)
    icon = models.ImageField(
        verbose_name='Иконка сервиса',
        upload_to='services/',
        blank=True,
        validators=[FileExtensionValidator(allowed_extensions=['png'])],
        default='services/default.png'
    )
    is_active = models.BooleanField(verbose_name='Сервис доступен', default=False)

    def __str__(self):
        return self.title


class Class(models.Model):
    title = models.CharField(verbose_name='Класс занятий', max_length=50)
    direction = models.ForeignKey('Service', verbose_name='Направление класса', on_delete=models.PROTECT)
    description = models.TextField(verbose_name='Описание класса', max_length=200)
    duration = models.FloatField(verbose_name='Продолжительность занятия в часах', default=1.0)
    picture = models.ImageField(
        verbose_name='Изображение класса',
        upload_to='services/classes/',
        blank=True,
        default='services/classes/default.jpg'
    )
    coaches = models.ManyToManyField(
        User,
        verbose_name='Тренера',
        limit_choices_to=Q(groups__name=COACHES_GROUP_NAME)
    )
    is_active = models.BooleanField(verbose_name='Класс доступен', default=False)

    def __str__(self):
        return self.title

# class membership card
