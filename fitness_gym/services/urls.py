from django.urls import path
from . import views


urlpatterns = [
    path('', views.ServicesList.as_view(), name='services'),
]
